function tinhGiaTienKmDauTien(loaiXe) {
  var giaTien;
  if (loaiXe == "uberCar") {
    giaTien = 8000;
  } else if (loaiXe == "uberSUV") {
    giaTien = 9000;
  } else {
    giaTien = 10000;
  }
  return giaTien;
}

function tinhGiaTienKm1Den19(loaiXe) {
  if (loaiXe == "uberCar") {
    // return ~ kết thúc function và trả giá trị nếu có
    return 7500;
  }
  if (loaiXe == "uberSUV") {
    return 8500;
  }
  if (loaiXe == "uberBlack") {
    return 9500;
  }
}
function tinhGiaTienKm19TroDi(loaiXe) {
  switch (loaiXe) {
    case "uberCar":
      return 7000;
    case "uberSUV":
      return 8000;
    case "uberBlack": {
      return 9000;
    }
  }
}

function tinhTienUber() {
  var loaiXe = document.querySelector('input[name="selector"]:checked').value;
  var soKm = document.getElementById("txt-km").value;

  // giá tiền theo từng loại xe
  var giaTienKmDauTien = tinhGiaTienKmDauTien(loaiXe);
  var giaTienKm1Den19 = tinhGiaTienKm1Den19(loaiXe);
  var giaTienKm19TroDi = tinhGiaTienKm19TroDi(loaiXe);
  var tongTien = 0;
  if (soKm <= 1) {
    tongTien = soKm * giaTienKmDauTien;
  } else if (soKm > 1 && soKm < 19) {
    tongTien = giaTienKmDauTien * 1 + (soKm - 1) * giaTienKm1Den19;
  } else {
    tongTien =
      giaTienKmDauTien * 1 +
      18 * giaTienKm1Den19 +
      (soKm - 19) * giaTienKm19TroDi;
  }
  document.getElementById("divThanhTien").style.display = "block";
  document.getElementById(
    "divThanhTien"
  ).innerText = `Tổng tiền : ${tongTien} VND`;
}

// uberBlack 18km
